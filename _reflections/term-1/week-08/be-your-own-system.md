---
title: 08. Living with ideas
period: 19-25 November 2018
date: 2018-11-25 12:00:00
category: term1
published: true
---


### Workshops.


### Day 1:


**Name:** Smell memory book.

**Description:**  what if our memories where olfactory?

![]({{site.baseurl}}/li1.jpg)



**Reflection:**

After battling to get out of the concept of water related to the face towel that I selected to do experiment and talking a bit with Angela to get out blockage. I thought of the question, what if paper didn’t exist? And that led me to think about the uses of paper and how it can be replaced with fabric. Then I came up with the concept of a picture book, and if I replaced that paper with fabric, what would be the meaning of it. So, what if our memories where olfactory? We could storage them on fabric. That led to think about consequences, how to preserve them, so that they last trough time. How to infuse them on the fabric.

### Day 2 :

![]({{site.baseurl}}/li3.gif)


**Reflection:**

The experiment with the inverted green screen was a great experience. Especially I liked a lot the way Angella figured out how to do an experience with technology that doesn’t exist yet, she managed to get as closed as she could to her what if. So you don’t need to have the technology to experience something. Also passing the gimmick face, and doing an experience for  so long to see in depth how it can affect you.


### Day 3:


![]({{site.baseurl}}/li2.jpg)




**Name:** I amer

**Description:** independence- the need to be self-reliant.

![]({{site.baseurl}}/li1.gif)



**Reflection:**

what if we had this magic box that could give us everything we need?
What are the implications of this device. Would it make friends for you? What would change in human relations? In the use of the world and its resources? Do we already have an unconciouss machine that give us everything we “need”? what do we “need”?

**Name:** knowlignator.

**Description:** curiosity- the need to acquire knowledge.

![]({{site.baseurl}}/li2.gif)


**Reflection:**

what if we could inject knowledge?
Would it give us the experience of that knowledge also? Is knowledge the way or the aim in the learning process?

### Day 4-5:

**A first person experience:**

The fear of stranger.  I believe that people are just like you, most people are “good” people, but because the “bad” is more advertised, in the news for example, or when you are a child and your mom tells you “don’t talk to strangers”; we create that barrier of trust between us. I wanted to know what feelings are triggered when trying to approach strangers. I did the experiment with 3 kind of strangeness: familiar (someone you know but they are not your friends yet), acquaintance (people you see sometimes) and total strangers. And 3 ways of contact-trust: sharing a secret (or a personal fact), human contact (a hug) and the physical object (lend an object important to you).

<iframe width="560" height="315" src="https://www.youtube.com/embed/mpniONprrxA" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

For the familiar level I didn’t had much problem, nothing was particularly triggered, mostly because I am not a shy person, and we have built a trust component in these 2 months with my classmates.

In the acquaintance level I had a failed hug, it was ok but still it made me feel a little shame, and maybe sometimes we don’t approach or trust someone because we fear that rejection, the key is to see that is something in them, not in you.  The rest of the experiment was ok, I don’t have problems hugging someone, as I have said, for Latin American people the human contact is not a big barrier, our personal space is not that hard to share. For the secret was a little hard, I thought what if she judges me, now I have to see her again and she doesn’t know me, probably would be a little weird that she knows a secret about me, maybe she will tell someone else. And for the personal object wasn’t so har either.

In the stranger level came the real revelations about how much distrust we have on people, specially of you are a woman, I was on the street looking for someone to hug, and not that unconsciously I selected a girl, like me, a lot of fear comes from the different, I had trouble thinking about hugging a stranger and maybe older men, and probably most of the men I saw are nice people, but a lot of what if came to me. I didn’t do the secret with the stranger, I think that telling some you will never see again wont be that hard, and for the personal object if I was going to get it back it should be a stranger that I could see again, so I choose a store that I go sometimes, so is not totally stranger but it doesn’t qualify as acquaintance either. And obviously I lend her something that doesn’t have that much value. I am an only child, and I think my sharing is not that developed, and I have some objects that are just too precious for me to risk losing them.

The experience showed me some things, the unconscious fears that I have, the fear of rejection, the way we catalogue people and prejudge them, and the levels of closeness we can share. Maybe with more variables trough more time and passing the gimmick phase I could understand better what rules of human behavior we can trigger in order to get pass that fear and believe ourselves as part of a hole where in most cases, people are just like you.
