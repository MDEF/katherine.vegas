---
title: 11. From Bits to Atoms
period: 10-16 December 2018
date: 2018-12-16 12:00:00
category: term1
published: true
---


### Our project was called SASSY PLANT.


We wanted to give nature the agency to “speak” for itself. To make awareness that it exist, is here, and want for us to stop our current behavior towards it and change.

For me nature is not this thing that is outside and afar from us. That notion I think is part of what led us to be in this stage of crisis. We are nature too, and everything that is in this world in nature. Is not only the green in forests, as we think when we say “I need some time in nature”. But still I liked this project to give plants a way of making a stand, or even better, to make people close to them.

For me this week was really good because I could mixed all that we learned in the weeks 3 and 7. I think it was a good way to glimpse at how fab academy will help us to make our projects into the physical world and to get all the tools that will help us in the future of our professional lives.  


#### More info [here.](https://mdef.gitlab.io/sassy-plants/)


**My contribution to this project was to make the cad files for the laser cut.**

![]({{site.baseurl}}/ba1.JPG)
![]({{site.baseurl}}/ba3.gif)


**To make the 3D file of the parts to attach the wheels to the servo motors.**

![]({{site.baseurl}}/ba4.jpg)
![]({{site.baseurl}}/ba2.gif)


**And to make the final connections for the electronics**.

![]({{site.baseurl}}/ba5.jpg)



### Final:

![]({{site.baseurl}}/b6.gif)
