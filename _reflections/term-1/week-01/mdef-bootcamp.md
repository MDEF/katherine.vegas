---
title: 01. MDEF Bootcamp
period: 1-7 October 2018
date: 2018-10-07 12:00:00
category: term1
published: true
---


![]({{site.baseurl}}/brain.gif)

In this week bootcamp I began to learn about how the program is going to develop, realized everyone’s background and interests are very singular but also have a lot in common. Most of my class mates are more interested in a social and community approach, and mine is more individual. But still are on the same page, change making, sustainability, circular economy, processes, nature. We formed groups and I end up in the social cohesion one, but I change to the process group which allows me to be closer to what I want to explore.

Git lab so great, it was really nice to begin exploring this code world that is new to me, but I am really interested. Later in the week we got a peer class from Ilja about this and we really saw the potential of this tool. I am really exited on working more on this. Mariana’s talk about communication, documentation, git and references was really good, she gave us some useful tools on this subject and helped us to understand how git works and how it can be useful to us.

I discovered all these hidden places in poble nou, Leka restaurant was great, I love the idea of the open source restaurant and will love to know more about the process of designing this place. Indissoluble studio was great, but is in my line of previous work, so wasn’t new for me, still the brain installation was amazing, got me thinking about how to merge more the interactive scenarios, mixing with science and experience. The transformation lab and the biciclot were very interesting not only for the amazing things they do but about their message and the experience people can get there, and how we can get more people to know about it. But also, about this debate I have between the old and the new and how to reconcile it, the reuse with having good and well-made objects.

The ‘trash’ collection was an amazing experience, for me as a Latin American was really shocking how someone can throw out something in perfect state instead of selling it. But was nice to see that people knows it can serve to someone else, I see a bit of solidarity in this. Also, the jobs that are based on this activity at such a grand scale. Let’s see what we’re going to do with this stuff that we gathered.

In the group discussion about the Week I discovered a lot of points of view from everyone that looks trough their interest, and that enriches my vision of the all.  Mariana’s talk about the map of poble nou really finished to change my perspective of the neighborhood.  I want to explore more on these places.

From Tomas’s previous work I learned more about what fab labs can do, all the city potential, all the problems with the current way of production and distribution, so many new possibilities that are emerging with all of these projects. I reflect on the debate on human consciousness, individual interventions in the city, changes that need more urgency than others.

From Oscar’s work I learned about all these possibilities between textiles and electronics, interaction, smart products. The issues with ultra personalization and acceleration on production. The importance of an interdisciplinary approach, to create from the physical exploration, the context and the imaginary.

I see great potential in their symbiotically mix of backgrounds and approaches. Between them and the diversity of the group I believe great things can be achieved.

Extracurricular: We went to valldaura campus. What an amazing place with so much <span style="color:magenta">potential!!</span>



![]({{site.baseurl}}/bc1.jpg)
![]({{site.baseurl}}/bc2.jpg)
![]({{site.baseurl}}/bc3.jpg)
![]({{site.baseurl}}/bc4.jpg)
![]({{site.baseurl}}/bc5.jpg)
![]({{site.baseurl}}/bc6.jpg)
![]({{site.baseurl}}/bc7.jpg)
![]({{site.baseurl}}/bc8.jpg)
![]({{site.baseurl}}/bc9.jpg)
![]({{site.baseurl}}/bc10.jpg)
![]({{site.baseurl}}/bc11.jpg)
