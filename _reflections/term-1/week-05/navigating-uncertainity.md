---
title: 05. Navigating the Uncertainity
period: 29 October - 4 November 2018
date: 2018-11-04 12:00:00
category: term1
published: true
---


With the lecture of Jose Luis de Vicente I started to think where did it all went wrong, did it?  I am a big fan on taking out the moral values of things, as we discussed later with Mariana Quintero. There is no doubt that today we face a really big challenge caused by our previous actions, but at least for me is the perfect opportunity to create something new, better and improved if we really get more conscious about our world, our impact. There is a scene from “mad men” a tv series situated on the 50’s that is stuck on my mind, the family is having a picknick on a park and when they leave the wife picks up the blanket and just throws everything on the grass and goes away.

<iframe width="560" height="315" src="https://www.youtube.com/embed/rhcKuMjvcCk" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

It’s stuck in my head about two things, one, possibly in that times, all the garbage wasn’t so harmful for the environment, as it is now; but also because as in the past 50 years we have done tremendous harm to the planet, we also have raised tremendous awareness about it, even if some people are still denying it, so for me we still have hope. I am not a hopeless dreamer about a perfect utopia, I am well aware of both sides of human beings, but I also don’t believe in those dystopias mad max style. I think we can have a little of all the decretionists, capitalists, accelerationist, extintionist and mutualists theorys that exists. As I have said, nothing is all wrong or right. But humans do have a very nice side and can come together in the face of self-annihilation. The real question is (as said for this master course) what kind of future we want to live-built-design for? And for that, if we want to be in a great future, as Tomas Diez said, we have to be optimistic and opportunist

### How can we get there? Or begin to get there:

As the lecture of Pau Alsina showed us, (and I have thought for a long time) design shouldn’t be view as a commodity, luxury or disposable practice. We build the world from the start, we gave shape to our environment, and that has the economic, political and social implications as the base and core of our world. I am always been fascinated by the human behavior, and actually started the undergraduate school of philosophy, wanting to go deep in that field. For many reasons I dropped off. But still one thing remained, my constant love to understand the human behavior. Because of that, when I changed to interior design (that was also my hidden passion since very short age) I always did it from a philosophical and humanist point of view, I always knew that design has the power to change the world, but as many other sides of humanity, is has lost its way. So, I find myself in this program finding all the things I thought about design been shared (and better studied and explained) by other professionals from various fields, and I cant be anything but happy, the world is changing, humanity will thrive, and it will do so improving itself. As long as we do something about the current status.

After diving in interior design, a little voice inside my head told me that experience is the approach that has been missing in a lot of design processes. Not the experience design of UX, or the experiences designed to promote a brand, but the real experience of processes that can help to help our world. In the last day, the talk with Oscar Tomico and the debate with my classmates, lead me to be a little confused, how can I explain my view, people are not understanding me at all. And then from a quote on one of the slides in this week “experience goes where data cant”, I found Superflux and listening to Anab Jain ted talk “why we need to imagine different futures”, she mentioned a project about the energy strategy of the United Arabs Emirates 2050, in which they had one a scenario about a sustainable future and they stated the impact of driving cars among other things, and how one of the participants told her “I cannot imagine that in the future people will stop driving cars and star using public transport, there is no way I can tell my own son to stop driving his car.” As they where prepared for that king of answers with a device that simulates the quality of air in 2030 if our behavior stays the same, only one smell of that device brought home the point that no amount of data could.The next day they announced an investment to improve those politics. And that is what I want to do! I believe experiencing thing can make awareness more quickly than data and discourses, changing patrons of human behavior is easier if we think about the experience of doing one thing or another, developing and designing from experience can lead us to more effective, sustainable and friendly society.

### Reading list:

-	About the minister of the future.
-	Durkheim collective consciousness
-	Saskia Sassen
-	Yuval Noah the myth of freedom
-	The radical idea of a world without jobs
-	All of Superflux lectures specially “learning to play with tomorrow”, “design for anxious times” and “design for the new normal.
-	How to thrive in the next economy
-	Taking a part: the politics and aesthetics of participation in experience-centered design.
