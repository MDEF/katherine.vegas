---
title: 10. An Introduction to Futures
period: 3-9 December 2018
date: 2018-12-09 12:00:00
category: term1
published: true
---

### Reflection:

The most important thing for me this week was learning to rethink using all the tools and theory we saw, in this case of the futures, although I believe we should do it with everything, to really search for originality and stop reusing ideas (kitsch), like the history of the futures,  analyzing trends, understanding how changes change, what theories and movements exist, systems, the 3 tomorrows, how to research and build scenarios . The future doesn’t exist, is not predestineated, we are not doomed, we have the power of making change. We have so much constructed in our collective mind about what the future will be that we don’t know how to properly think about it and come up with scenarios and responses to them. **The future cant be the future of yesterday.**

Technologically speaking we have developed so much, and change from what we thought of it, I remembered words from my father by the image of The Jetsons that Elisabeth Roselló showed us, in which he said to me in reference of that show and many other future scenarios in commercial television and movies, “we believed that our index finger was going to be the protagonist of managing our interfaces, but now everything is done by the thumb”. And we can see it in how older people use smartphones for instance, you always see them using the index, at least the ones that are not really aware of it like my dad, contrary of younger generations, but everyone got them at the same time, so it was that construct on their minds that lead them to use that and not the other finger. And that’s how every use, object, technology, space, product or future will be build, and used. Our unconscious mind will make us do things one way or another, unless we are more conscious about it, as I said before in the discussion about free will.

So we, as future futures makers, have to begin to unthink and develop scenarios that are new, also because I am a firm believer that we can make something happen if everybody thinks that’s the way its going to be, that’s one of the reasons I always talk positively about the future, to try and break those apocalyptic constructs ( I know also we have to break with naïf utopias although that won’t bother me that much, depending on the kind, technological ones are bad in my opinion) that we have had for so long time and are really embedded in our minds, and I am not saying that the issues are not real, and that we have to do something about it ( I wouldn’t be here), but we have to change, and think differently to be able to create a new positive futures. **Now.**

I was also captured from the quote “it is easier to imagine the end of the world than to imagine the end of capitalism”. And maybe that’s one of the reasons we can’t imagine a new world, a new way. As M. Thatcher said, “there is no alternative” and we live in that kind of world. But if we do the exercise, we as responsible creators, to rethink and get out of that constructs about systems, futures, morals and ways, maybe **there is more than a beacon of hope for us in this world.**

### Exercices:

We constructed a scenario based in two axis, the bizarro and the de-colonized.


![]({{site.baseurl}}/f1.jpg)



We analyzed the trend about Social and Economic inequality. We saw how everything is interconnected and then compare and connected with other trends analyzed by my classmates.

![]({{site.baseurl}}/f2.jpg)


### Conclution:

Now I have the tools to analyze the trends that are coming regarding my area of interest, and how maybe will affect in future scenarios, the technology part of the VR and augmented reality, but also how we will live, the quality of our lives, how social phicology will affect us, the need to scape will behave differently as we develop different tools, the addiction may be see and approach on based on new neuromedical insights, and maybe the social media streaming trend will provide us with something useful.
