---
title: 07. The Way Things Work
period: 12-18 November 2018
date: 2018-11-18 12:00:00
category: term1
published: true
---


### Technology is a way to express anything, make the world what we want it to be.


Dismantle everyday objects allowed me to realize the simplicity of some of them, and how everything work. The same principles and conventions are used to build almost anything we use, so, hacking it, transform it, change the use is not that hard once you understand those guidelines.

![]({{site.baseurl}}/wtw11.jpg)


### Workshop.

In our group we decide to create an input: ROOM MOOD SENSOR.  We figure we needed al least 3 variables: people in the room, amount of noise and amount of light; to determine 3 kind of moods: exited, focused and relaxed. We shared tasks to divide the work but rotated them, so everyone could work on everything.

- People + light + noise = exited
- People + light – noise = focused
- People - noise - light = relaxed

We did two sensors located in different parts of the room, one on the center for the room that measured both light with an LDR sensor and noise with a microphone and one on the door that measured if people came. For this last one we tried with an ultrasonic sensor and with an infrared sensor ending up doing it with just a regular LDR that sensed the shadow of a person coming in.


![]({{site.baseurl}}/wtw22.jpg)
![]({{site.baseurl}}/wtw3.gif)


I saw nice outputs by my classmates like the balloon pumping and the led moving, also the input with the plant as a capacitor.
The combination between them using the network created by the Raspberry Pi  was amazing, touching the plant can create a noise with the air pumping output, or entering the room with our sensor could make the balloon inflate.

![]({{site.baseurl}}/wtw1.gif)
![]({{site.baseurl}}/wtw3.gif)



### My reflection:

One of the most interesting things for me this week was learning about the real principles upon which internet was build. And learning about copyright, copyleft, copyfree and creative commons. Internet was built on the principle of peer to peer, and somehow like all things in our capitalist system ended up being managed by this big Silicon Valley corporations, as always the creators think one way and the industry in another. I do believe it should be peer to peer storage of small portions and decentralized from big corporations that makes all the decisions, maybe similar to blockchain, but with some adjustments.
Then there is the issue of intellectual rights. And I like this topic because I find it similar as what happens in the design industry, and how can we make everything more accessible and democratic.

![]({{site.baseurl}}/wtw33.jpg)


So we all know what **copyright** means, grants the creator of an original work exclusive rights for its use and distribution.

![]({{site.baseurl}}/wtw55.gif)


**Copyleft** and its 4 freedoms (based on “free as in free speech not free as in free beer”) gives credit to the original creator.

-	The freedom to run the program as you wish
-	The freedom to study and change it.
-	The freedom to redistribute copies (and earn economic revenues)
-	The freedom to distribute copies of your modifications.

**Creative commons** enable the free distribution of an otherwise copyrighted "work". A CC license is used when an author wants to give other people the right to share, use, and build upon a work that he or she (that author) has created. CC provides an author flexibility (for example, he or she might choose to allow only non-commercial uses of a given work) and protects the people who use or redistribute an author's work from concerns of copyright infringement as long as they abide by the conditions that are specified in the license by which the author distributes the work.

![]({{site.baseurl}}/wtw4.png)



**Copyfree** is a term used to identify the freedom to copy, use, modify, and distribute what you possess. It is a philosophy that stands in contrast to both copyright and copyleft, in that it does not seek to limit or restrict your rights regarding your possessions at all. Copyfree is not about limited monopoly on the product of the intellect like copyright, nor is it about dictating terms of redistribution like copyleft. It is a policy supporting control over what you possess and allowing others to control what they possess, compatible with motivations for such freedom grounded in creative, legal, philosophical, and technical needs, among others.

In all disciplines of design, we can see the issues with original work. As design is often perceived and distributed as a luxury commodity, prices of designer’s work (furniture, clothes, objects or spaces) are often way out of price range for most people. So, there it develops this sub world of cheap imitations of those work products.  With cheaper materials and low-quality manufacturing, or by copying the style in a space, or with retailers like Ikea; these copies of original work can be more accessible for the common people, but then we all end up living in this kitsch world. And yes, a lot of the times the products of a well-recognized designer are just too much, but a lot of times, and more in this current age, we can find affordable designer’s products. Products well designed, maybe from recycled materials. And still people don’t consume them based on that subconscious assumption that it is a luxury.
Getting to the point that this author rights got me thinking about, design should be manifested as the internet once was, and should be again, a free world of sharing and making, so people can have quality on all design products, but designers, as developers of any kind, still have to get revenues from their creations.

#### How can we achieve these goals of balancing democratization on any kind of creations, and revenues for their creators? Can these rights licenses help us? Let me get back to you.
