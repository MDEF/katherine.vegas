---
title: 4-Electronic Production
period: February 20
date: 2018-12-16 12:00:00
category: fabacademy
published: True
---

### Assignment:

For this week we were asked to do a PCB from scratch. First I downloaded the file given to us, hello.isp.44 traces and outline, and used fabmodules to calculate and get the code for the machine, in this case a .rml file, like all the machines laser cutting, cnc, and this milling one what the machine uses is code that get the coordinates, speed and strength to be used.

![]({{site.baseurl}}/hello.ISP.44.traces.png)


 So I milled a PCB from the design given to us.
 To make the file for the machine we use fabmodules, I uploaded the png file given to us and set it up for the traces or outline. Set it up for the roland mill and then downloaded the file to upload on the machine software.
 To set up the machine I needed to use a 1/64 mill for the traces and a 1/32 for the outlines, the X and Y 0 are set up manually trough the machine controls, and the Z is set up by realizing the mill, this process can break the mill so I needed to be careful.

 ![]({{site.baseurl}}/fabmodules.jpg)
 ![]({{site.baseurl}}/roland.jpg)
 ![]({{site.baseurl}}/roland2.jpg)


Result:

![]({{site.baseurl}}/pcb2.jpg)


Then pass to gather the components:

-1 ATTiny 44 microcontroller
-1 Capacitor 1uF
-2 Capacitor 10 pF
-2 Resistor 100 ohm
-1 Resistor 499 ohm
-1 Resistor 1K ohm
-1 Resistor 10K
-1 6 pin header
-1 USB connector (was not available)
-2 jumpers - 0-ohm resistors
-1 Crystal 20MHz
-2 Zener Diode 3.3 V

Then solder them into the PCB.
One of the biggest issues I faced was needing an extra hand, when you need to have the solder, the iron and hold the component in place, but I manage to do it putting some solder on place first and the heating it again to place the component.  

![]({{site.baseurl}}/pcb1.jpg)
![]({{site.baseurl}}/pcb3.jpg)



#### Result:

![]({{site.baseurl}}/pcb4.jpg)

#### programming:

I couldn't program this board, still don't know why, I follow the [tutorial](http://fabacademy.org/2019/docs/FabAcademy-Tutorials/week04_electronic_production/fabisp.html#install) given. Installed all the software necessary for windows, with the programmer connected to the board, I never got the green light, the board was not able to be read by the computer, I tried resoldering the ones that look shady, also heated with the hot air all the board to make the soldering stick better but still didn't make it. Because we couldn't figure out why the board wasn't being read by the computer we (me and the tutors) decided to leave it.
