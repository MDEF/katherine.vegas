---
title: 3-Computer Controlled Cuting
period: February 06
date: 2018-12-16 12:00:00
category: fabacademy
published: true
---

### Laser Cut:

For this week I did a press fit kit in Autocad that you can see in [week 2.](https://mdef.gitlab.io/katherine.vegas/reflections/faw2/)

After the file was ready I cut it. I use cardboard 5 mm and checked the values needed for this material in this machine, then I fix the laser by doing the focus and started cutting.

![]({{site.baseurl}}/cut1.jpg)
![]({{site.baseurl}}/cut2.jpg)

Then I assemble it. The joins I decided to use were very efficient for the angle and design I chose. There was no need for glue or tape.

![]({{site.baseurl}}/box3.jpg)
![]({{site.baseurl}}/box4.jpg)

This press kit can also be used for organizing a drawer. I like to do things that have a purpose.

![]({{site.baseurl}}/box2.jpg)

#### Group tests

We did a press fit test with different kind of joints to check how they can be used in different purposes.

![]({{site.baseurl}}/Pressfit.jpeg)
![]({{site.baseurl}}/Pressfittest01.jpg)

We did a Kerf test to check the margins and tolerances needed using the machine and the cardboard.

![]({{site.baseurl}}/kerf.png)
![]({{site.baseurl}}/kerfc.jpg)

Finally we did a raster test. To raster and to engrave is to mark (burn) text, images or anything but not cutting it. We used a photo of our classmate, but you can use anything you want.

![]({{site.baseurl}}/raster.jpg)

### Vinil cut:

For this assignment I downloaded a vector for a mototrbike, you will find the file in the last part of this post.

![]({{site.baseurl}}/MOTO.jpg)

Then I saved it in ilustrator 8, but I got an error.

![]({{site.baseurl}}/problem.jpg)

We tried opening in rhino but it also didn't worked.

![]({{site.baseurl}}/rhinocut.jpg)

Then we did a .pdf and open it in inskape, it worked, and we copy paste it in cutstudio.

![]({{site.baseurl}}/cutstudio.jpg)

I put the vinyl in the cutter, set the roll mode and it calculates the lenght and width automatically.

![]({{site.baseurl}}/cutsetup.jpg)

Then cut it, transfer with scotch tape and paste it.

![]({{site.baseurl}}/vinilcut.gif)
![]({{site.baseurl}}/transfer.gif)
![]({{site.baseurl}}/paste.gif)


![]({{site.baseurl}}/finalvinyl.jpg)

I took out some parts that should have been closed on the original file, one thing is a drawing, and another is the cut, too thin lines will be just cutted, like the handle bars that I replaced with other piece because it wa no material in the cut.

#### Files:

-[Illustrator vector ]({{site.baseurl}}/MOTOvinilpdf.ai)

-[PDF editable]({{site.baseurl}}/MOTOvinilpdf.pdf)
