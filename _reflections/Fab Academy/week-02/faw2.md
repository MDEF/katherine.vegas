---
title: 2-Computer Aided Design
period: January 30
date: 2018-12-16 12:00:00
category: fabacademy
published: true
---
Computer aided design has been a base in my life from 10 years on now, I have explored a lot of software tools to help the work of designing interiors.
My personal favourites are AutoCAD, since is a very intuitive interface, precise, and full with commands to make it easy, but for the 3d modeling, I feel it comes short. Being who I am, for modeling in 3D I found Sketchup the best, is easy and the interface is very intuitive with the icons, has a lot of models in the warehouse library, but also falls short when you need to do complicated meshes or curved designs.
3dMax I have always tried to learn, and fail because of the complicated interface.
Rhino as been a very nice discovery through out this course, I haven't tried it before because I thought it was going to be similar to 3Dmax, but I now found it has the goodness and easy way to use like Sketchup and the commands like AutoCAD, but adding the good treatment of curved meshes and vectors. I want to learn more of it.
Fusion 360 has also been a very nice discovery, for parametric design with the autodesk interface, but I still have to learn more from it

### Assignment:

For this week we were asked to do a 2D or 3D model of our project. Since I have no project yet. I did the group press kit test on fusion trying to learn the new software.

started by doing a rectangle.
![]({{site.baseurl}}/fusion1.PNG)

Then extrude it.
![]({{site.baseurl}}/fusion2.PNG)

Add rectangles in the side of the rectangle and extrude it too.
![]({{site.baseurl}}/fusion3.PNG)

Add more rectangles to have different lengths and test and push forward the main one.
![]({{site.baseurl}}/fusion4.PNG)

add a triangle shape also for testing different joints
![]({{site.baseurl}}/fusion5.PNG)

create another rectangle and extrude it, to make the negative one in which it will press fit.
![]({{site.baseurl}}/fusion6.PNG)

Join them
![]({{site.baseurl}}/fusion7.PNG)

Create a union and select cut to take out the shape from the first one to the second one.
![]({{site.baseurl}}/fusion8.PNG)


Also did a 2D plan for next week laser cutting, a press fit kit. I am learning Fusion 360, but didn't manage to make it parametric (still working on it) so I did it in AutoCAD for time reasons, as I said before AutoCAD is my right hand, I can make everything very fast, so I choose to use it. I did all the outlines, then did one of the joins, copy it and trim to have the holes, then join all the polylines to have closed shapes. Then did the rectangle of 1.2X1.2M as half of the board and place them giving a bit of tolerance for the machine but using as few material as possible.

![]({{site.baseurl}}/box1.jpg)

#### Files:

-[CAD box]({{site.baseurl}}/box.dxf)

-[fusion presskit]({{site.baseurl}}/presskitfit.f3d)



#### Bonus:

I have the advantage of being an interior designer, so I am going show 2 of my previous work with 3d modeling and rendering, obviously is not a fabrication piece but they show my level of handleling a 3D modeling program. For rendering I usually work with a rener motor called Vray, in which you can create the textures and then manage the lights for the scene you want to render.

![]({{site.baseurl}}/render2.jpg)
![]({{site.baseurl}}/render1.jpg)
