---
title: 6-Electronics design.
period: January - June 2019
date: 2018-12-16 12:00:00
category: fabacademy
published: true
---

For this week the assignment was to design a PCB from scratch using Eagle or Kidcad. I used Eagle since I am a fan of Autodesk. I found this process very easy to understand in the way that is the same concept as designing any electrical network for a space but it has the complexity of making everything fit and connect in the space of the board without using a lot of jumpers, so that can take some time, still is very nice to know how to create and personalize any board to our requirement in the future and not being stuck with what is on the market.

I had to download the fab library to have the components to add in the design. you can download it [here](https://cba.mit.edu/calischs/libraries/blob/27fcf5b00271f3033248c3d714614e2b90541a71/eagle/fab.lbr).

 After adding the library you add the components in the schematic design:
-6-pin programming header
-Microcontroller: Attiny44A
-FTDI header
-20MHz crystal
-Resistors - 0 ohm resistor the jumper. 499 ohm resistor for the LED, 10K ohm ----resistor for the button
-Capacitors 2 x 22 pf capacitors
-Button
-LED
-Ground
-VCC.

#### The schematic design:

![]({{site.baseurl}}/schematic.PNG)

After organizing the schematic I move to the board part of Eagle, were I started the design part of the board, making everything take place and connect in the most efficient way, it takes some time to be able to get everything in place and avoid using jumpers ( 0ohm resistors) as much as you can, taking into account some rules, capacitors need to be close to the microcontroller, . The rastnest command helped me. Also I needed to be careful with the size of the pathway, not too small so it will not connect properly. you can check everything wih the DCR command.

#### The board design:

![]({{site.baseurl}}/image_def.png)

Then you export the png image, 1000 resolution and monochrome.

![]({{site.baseurl}}/export.PNG)

#### The PNG for milling:

![]({{site.baseurl}}/traces_def.png)
![]({{site.baseurl}}/outline_def.png)


After having the design done came the same process as in [week 4](https://mdef.gitlab.io/katherine.vegas/reflections/faw4/) to make with fabmodules the images for the machine to mill the PCB.

I had some issues cause the tape used in the back of the board to fix it to the machine was not strong enough, so it moved and broke the board. I had to try 3 times to get the end result, is very important that the board is really stuck to the machine. Also the design of the machine in the place where you put the mill is the worst, for fixing the Z that you need to drop the mill and then secure it again can take time.

#### The result:

![]({{site.baseurl}}/pcbdesignfinal.jpg)

#### Files:

-[Board Eagle]({{site.baseurl}}/board_def.zip)
