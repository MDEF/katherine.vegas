---
title: Material Driven Design
period: January-March 2019
date: 2018-12-16 12:00:00
category: term2
published: true
---
### Offline and online approaches.

In the first day of experimentation trough material driven design we got to the Valdaura Labs to learn from a natural environment and a natural sourced material as wood is.
For me being in contact with the forest and its components and behaviors was very close, since I am a landscaper designer I've ad the fortune to learn how to design with a constantly moving element as plants are. So I have been always very conscious about the difference between designing with a static element (as in interior design is done) and the changing one, where you need to take into account how in 5, 10 or 30 years is going to behave. I found a relation between what the class was about and that that I experience in my professional and academic life. Because the best designs (in landscaping) are the ones that are made from the material (terrain, plants) and not the other way around.

We carve a spoon from the log of wood. I had laurel wood that is not hard and has an amazing smell.

![]({{site.baseurl}}/mdd01.jpg)
![]({{site.baseurl}}/mdd02.jpg)

#### Conclusion on the day.

Doing a hand craft in something that I have never done before got me thinking a lot about the offline approach to a task. Maybe because I had some years of professional experience in a field that most of the work is done behind a computer (apart from the supervising visits on the construction site) I have not learn to do a new task in quit some time. Until the master begin. So I had encounters of this kind during some of the week last term, bur I was not conscious about the offline and online way to do something. Of being really aware on what you are doing, and how what you do affects what you are doing, all steps connected to a final result, that is totally out of your control, in the way that the material is "talking" to you. So maybe your original idea is change when you encounter something on the material, or on how to carve it.
Related to my project, it was a very good 1st person experience on how sometimes escape reality can also be done by being really into reality, as this task was, so maybe one of the solutions of that need to escape is somehow learn to be more present, maybe to un hack us we just need to re hack our brains in some new aware way to do things.

### Material Exploration.


The aim of our material design project was to create a product using a new biodegradable material, created by us without using chemicals through an abundant source available in the city. The project required to “live with the material”, meaning getting to know its properties by experiencing it in as many possible ways to discover possible uses and, in the end, a product.

The shrimp waste contains several bioactive compounds such as chitin, pigments, amino acids, and fatty acids. These bioactive compounds have a wide range of applications including medical, therapies, cosmetics, paper, pulp and textile industries, biotechnology, and food applications. Chitin is a polysaccharide sourced from exoskeletons: this means that it is abundant in the shells of crustaceans and insects. It is the second most abundant biopolymer on Earth, after cellulose.

Experimentations around the creation of a bio plastic from chitin started a few years ago with Shrilk, a fully degradable bio plastic developed at Harvard made with chitosan (a product of deproteinized chitin) and silk. Shrilk was tested to be used as a plastic substitute and also for creating implantable foams, films and scaffolds for surgical closure, wound healing, tissue engineering, and regenerative medicine applications .

Shrilk was discovered to be very expensive to source and produce, but chitosan alone - without silk- became the object of various experiments at the intersection of science and design. The most famous experiment around chitosan and its possible application as a bio plastic is the one performed at MIT’s Mediated Matter Lab: scientists created a wing-like multifunctional structure using a custom-made 3-d robotic printer, which extruded chitosan in different concentrations. The most interesting property of chitosan’s bio plastic is that it is not water resistant. Although the process of degradation is quite long, products made with this plastic eventually dissolve in water. Another interesting aspects of the material is that, even though crustaceans contain quite powerful allergens, the bio plastic is hypoallergenic.

![]({{site.baseurl}}/mdd1.png)

The first step into the project was to source the material. Shrimp shells are supposedly abundant in Barcelona because of tourism, the amount of restaurants and - I thought - paella. As me and Silvia (who worked at the project with me) discovered, it is not that easy to collect the shells. The first batch was a gift from our classmates, who collected it from a stand at the Boqueria. I then started asking around in restaurants and markets in my area - El Born - but people were reluctant to help me in my very specific task, which was to collect raw shells.

So instead of asking to the final recipients of shrimps, we started researching along the production line. Where does restaurants source shrimps? Where does fish stalls in the markets get them? We discovered that in the suburbs of the city there is a place called Mercabarna, the wholesale market of the markets of the city, where most of the food consumed in Barcelona comes from. Among many others, Mercabarna has a fish market which is open all nights (apart from Saturday), from midnight to six in the morning.

![]({{site.baseurl}}/mdd2.png)

Exploring Mercabarna

The trip to the market was a very interesting experience: we were the only girls, the only visitors that didn’t want to buy any fish. It was an important step, it allowed to:

Find a reliable source of shrimp shells from market stalls wanting to get rid of shrimp’s heads
Reflect on the concept of circularity: because Barcelona is a maritime city and because paella is such a common dish, I somehow implied that shrimps were fished locally. I had the opportunity to ask to the people working at the wholesale market, and I discovered that most of the crustaceans we come from the Western coats of northern and central Africa, specifically Mauritania, Gambia and Senegal. Intensive shrimp fishery brings along a wide variety of issues, among which the most important are trawling and its by catch and overexploitation.

#### Experiment

Once we found the source of material, we focused on the experiment. The goal was double: to extract chitosan from the shells and discover other possible material applications.

Chitosan is so widely use in medical research that it’s quite easy to find scientific references & articles online on material experiments. It was more difficult to find “in-between” references, meaning some examples of chitosan/shrimps experiments more focused towards design and the properties of the material. Anyway, reading academic papers we found out how chitosan is extracted from shrimps through two different methodologies:

As we noticed, there is apparently no way of extracting chitosan from crustacean shells without using caustic soda, a powerful chemical that we were willing to avoid. We were able to talk about this issue with some amazing students from RCA, collectively called The shellworks. The aim of their project, the only one we found at the intersection of design and research, is to design and produce specific machines to handle the chitosan bio plastic's properties and create objects. The shellworks confirmed to us that there is no way of extracting chitosan without chemicals. After a talk with Mette we decided to skip the extraction step, buy chitosan powder online and focus our efforts on the more practical side of the experimentation, meaning the creation of final products from chitosan. Nonetheless, we continued experimenting on possible application of shrimp shells outside of the chitosan scope.

![]({{site.baseurl}}/mdd3.png)

#### Recipes

![]({{site.baseurl}}/mdd4.png)
![]({{site.baseurl}}/mdd5.png)


#### Conclusions.

Focusing on chitosan allowed us to focus more on the final material and its possible applications, but at the same way it distanced us from the original source, shrimps. It was a pity but we had a short amount of time to complete the process.

Experimenting with chitosan plastic has been fun, mainly because it is such a strange material to handle. Initially the plastic is a gel, that then has to solidify through water evaporation. This means that whatever final product one has in mind, it is necessary to take the shrinking process into account when preparing molds for the bio plastic. At the same time, working with a gel can be challenging: because it’s such a watery form, it is difficult to create vertical structures such as cups. The gel will always tend to go back to the flattest possible shape.

These properties of the material required a lot of thinking about the molds and how to create shapes. The only reference in mind for me was the work done by The shellworks. To create a cup for example, they created a machine that heats a cup mold and then dips it into chitosan bio plastic, letting it solidify immediately. The process allows to create layers until the cup is sturdy enough to be ready.

I thought, since we are dealing with a bio plastic, how about making a mold as used in injection processes to give it a shape. So 3D printed a mold of a spoon, to make some connection to the start of the seminar. Results were not as expected since the recipe was not yet perfected, and it takes a lot of time to dry, also when it does it shrinks considerably. But I think it was a god way to learn more about the material, after some days it gets pretty hard.

![]({{site.baseurl}}/mdd6.jpg)

#### Reflections.

Changing the shift of getting to know the material first and then think of the object and not the other way around, also get rid of the concept of sheets was very hard but as well very liberating. For the hard part I believe it was mostly because, we as designers ( or I)
had some issues with the method, for me the scientific method is just not a way I can do things. I am not that rigorous, I am more the kind of lets try and mess up things and then we will see, ( never even follow a recipe while cooking). I am more inclined to the other method Mette mentioned, a more intuitive one, but that is not enough either. I do believe we are in desperate need for these experiments and explorations in materials to go the next step, but for that I think we need our own method, to get more designers on board, and to not get overwhelmed by scientist research or others work. A material driven design method.  I don't know, I just know at least for me, that was the one thing missing to connect better to this world, and to not stare at the shrimp powder for a long time thinking, ok what's next?. The experimentation was still amazing. Discovering how something behaves so different than others materials to the same process, why, and try to build from the mistakes. I had a very nice time. 

#### Bibliography

Chitosan bioplastic:

[- The Shellworks)](https://www.theshellworks.com/)


[- https://www.popsci.com/article/science/plastic-made-shrimp-shells](https://www.popsci.com/consent.php?redirect=https%3a%2f%2fwww.popsci.com%2farticle%2fscience%2fplastic-made-shrimp-shells)


[- Shrilk Biodegradable Plasti](https://wyss.harvard.edu/technology/chitosan-bioplastic/)c


[- neri oxman and MIT develop digitally produced water-based renewable material](https://www.designboom.com/technology/neri-oxman-mit-mediated-matter-water-based-digital-fabrication-05-14-2018/)


[- MIT Mediated Matter](https://www.behance.net/gallery/65182471/Water-Based-Digital-Fabrication)




[-  Water - Based Robotic Fabrication: Large - Scale Additive Manufacturing of Functionally Graded Hydrogel Composites via Multichamber Extrusion](https://core.ac.uk/download/pdf/83231644.pdf)

Chitosan preparation:



[- Efficient use of shrimp waste: present and future trends](https://link.springer.com/article/10.1007/s00253-011-3651-2)


[- Methods for extracting chitin from shrimp shell waste](https://www.ncbi.nlm.nih.gov/pubmed/9754407)

[- A new method for fast chitin extraction from shells of crab, crayfish and shrimp.](https://www.ncbi.nlm.nih.gov/pubmed/25835041)

[- Extraction of chitin from prawn shells and conversion to low molecular mass chitosan](https://www.researchgate.net/publication/257103033_Extraction_of_chitin_from_prawn_shells_and_conversion_to_low_molecular_mass_chitosan)


[- Extraction of chitosan and its oligomers from shrimp shell waste, their characterization and antimicrobial effect](https://www.ncbi.nlm.nih.gov/pmc/articles/PMC5352841/)


[- Preparation and Characterization of Chitosan Obtained from Shells of Shrimp](https://www.ncbi.nlm.nih.gov/pmc/articles/PMC5450547/)

Chitosan preparation by fermetation:



[- Fermentation of Shrimp Biowaste under Different Salt Concentrations with Amylolytic and Non-Amylolytic Lactobacillus Strains for Chitin Production](https://pdfs.semanticscholar.org/4e93/c9a952a504027c950071925c66d565e49926.pdf)


[- Chitin extraction from shrimp shell waste using Bacillus bacteria](https://www.sciencedirect.com/science/article/pii/S0141813012003509)
