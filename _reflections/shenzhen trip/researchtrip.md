---
title: Shenzhen- Guangdong- China
period: January 2019
date: 2018-12-16 12:00:00
category: shenzhen
published: true
---


Getting to know such a different culture for me was a challenging mostly because of getting rid of pre concepts and imaginaries about it, that for one we shouldn't have but somehow we end up creating even if we had not so much information about it.
Passing those pre concepts in china was very easy, mostly because the main differences are more in how we do things, and obviously on the scale that we do them, but I came to found that we are all humans after all and not that different, at least from my point of view.

I encountered a beautiful place, full with strong flavors, lots of smiles, and where they truly believe the sky is the limit and you can do everything that you want, or at least test it and see if it succeeded.
I do believe for my professional life the trip was very important. It gives you the chance to see how one of the biggest economic potency behaves, and understanding its ways, a little bit obviously it was a short visit, but it really open your mind, and gives you the understanding on why and how they are as important as they are and are going to be in the futures.

![]({{site.baseurl}}/shzh2.jpg)


I do believe we have to encounter a middle point between our way of over thinking, researching and super intellectualism and their way of developing ,testing and releasing with no regards on the impact. They get things done, but we have learned the hard way that society really needs to think about what and how we do it, even more now with pressing issues like AI, and climate change that needs cooperation in all levels and in all parts of the world or it really can't be tackled. And after all, as I said before, we are all humans, we have the same basic needs and desires, even if they are cultural different or have different concepts on things because of their uprising, in the core we are the same. That’s why we see them also glued at their phones, even if they don't have the usual social media they do have a version of everything, same patrons of addiction and needs of escape as we all do. And the most obvious, they do live in the same planet as we do, even if they forget about us or we of them. So we need them to get on board to some level of worry about this stuff, and not only keep developing, as we do need to get onboard their mentality of getting out of the thinking and into the doing.
As all in this universe is dual, and the best part of it is when they converge.

![]({{site.baseurl}}/shzh1.jpg)


I now see technology as symptom of dumbness and intelligence. It will reflect on their creators, and show humanity’s issues, not resolve them. That’s totally on us, we are still in charge.

![]({{site.baseurl}}/shzh3.jpg)
